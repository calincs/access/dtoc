import copy from 'rollup-plugin-copy';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import html from '@rollup/plugin-html';


export default {
	input: 'src/js/app.js',
	output: {
		file: 'dev/dtoc.js',
		format: 'iife',
		name: 'dtoc',
		sourcemap: true,
		strict: false
	},
	plugins: [
		nodeResolve(),
		commonjs({
			include: /node_modules/,
			requireReturnsDefault: 'auto'
		}),
		html({
			template: () => {
				return `
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>DToC</title>
		<script type="text/javascript" src="./lib/ext/ext.js"></script>
		<script type="text/javascript" src="./lib/saxon/saxon.js"></script>
		<script type="text/javascript" src="./dtoc.js"></script>
		<link rel="stylesheet" type="text/css" href="./lib/ext/ext.css" />
		<link rel="stylesheet" type="text/css" href="./css/dtc.css" />
		<script src="https://kit.fontawesome.com/891f15ee2f.js" crossorigin="anonymous"></script>
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&family=Roboto+Slab:wght@300;400&display=swap" rel="stylesheet">
	</head>
	<body>
<script type="text/javascript">
const config = {
	"ignoreNamespace": true,
	"documents": "//front|//div[@type='index']|//div[@type='chapter']|//div[@type='preface']|//div[@type='afterword']|//div[@type='contributors']",
	"documentTitle": "head/title",
	"documentAuthor": "docAuthor[1]",
	"documentImages": "graphic[@url]",
	"documentNotes": "note",
	"documentLinks": "ref[starts-with(@target,'http')]",
	"inputs": [
		"http://localhost:8888/data/xml/sample.xml"
	]
}
window.dtocApp = new dtoc({debug: true, dtocRootUrl: 'http://localhost:8888/', input: config, showMenu: true});
</script>
	</body>
</html>
`;
			}
		})
	]
};
