import Component from '../component.js';
import ReaderView from './view.js'
import DocLoader from '../../app/docloader.js';
import DToCToolTip from '../../app/tooltip.js';

export default class Reader extends Component {
	CUSTOM_CSS_URL = 'css/xml/custom.css';

	linkSelectors = [];
	imageSelectors = [];
	noteSelectors = [];

	tokenToolTipsMap = {};

	_docId = undefined;
	get docId() {
		return this._docId;
	}
	set docId(docId) {
		this._docId = docId;
	}

	constructor(app, config) {
		super(app);
		
		this.CUSTOM_CSS_URL = app.urlBase + this.CUSTOM_CSS_URL;
		
		this.view = new ReaderView({_controller: this, showMenu: config.showMenu});

		this.subscribe('documentSelected', function(src, data) {
			this.fetchDocument(data.docId);
		}, this);
		
		this.subscribe('docModelClick', function(src, data) {
			this.fetchDocument(data.docId, this.view._doScrollTo.bind(this.view, data.amount, true));
		}, this);
		
		this.subscribe('docModelScroll', function(src, data) {
			this.fetchDocument(data.docId, this.view._doScrollTo.bind(this.view, data.amount, false));
		}, this);
		
		this.subscribe('kwicSelected', function(src, data) {
			this.fetchDocument(data.docId, this.scrollToToken.bind(this, data.tokenId));
		}, this);
		
		this.subscribe('tagSelected', function(src, data) {
			this.fetchDocument(data.docId, this.scrollToToken.bind(this, data.tokenId));
		}, this);

		this.subscribe('tagsSelected', function(src, tags) {
			this.clearHighlights('tag');
			for (const docTags of tags) {
				for (const tag of docTags) {
					this.highlightTag({docId: tag.docId, tokenId: tag.tokenId, type: 'tag'});
				}
			}
		}, this);
		
		this.subscribe('indexesSelected', function(src, indexes) {
			this.clearHighlights('index');
			for (const index of indexes) {
				index.type = 'index';
				this.highlightTag(index);
			}
		}, this);

		this.subscribe('kwicAdded', function(src, data) {
			this.clearHighlights('kwic');
			if (!Array.isArray(data)) {
				data = [data];
			}
			for (const d of data) {
				this.highlightTag(d);
			}
		}, this);

		this.subscribe('clearSelections', function(src, data) {
			this.clearHighlights(data);
		}, this);
	}

	reset() {
		this.linkSelectors = [];
		this.imageSelectors = [];
		this.noteSelectors = [];
		this.tokenToolTipsMap = {};

		// this.view.readerContainer.load(this.view.urlBase+'data/xml/blank.xml');
	}

	fetchDocument(docId, callback) {
		if (this.docId !== docId) {
			this.docId = docId;
			
			const doc = this.getCorpus().getDocument(this.docId);
			this.view.setTitle(doc.getHtmlTitle());
			this.view.setText(doc.xmlString, doc.css).then(() => {
				if (callback) callback.call();
			});
		} else {
			if (callback) callback.call();
		}
	}

	reloadDocument() {
		const docId = this.docId;
		this.docId = undefined;
		this.fetchDocument(docId);
	}

	scrollToToken(tokenId) {
		const doc = this.getReaderDoc();
		let tag = Ext.get(doc.querySelector('*[tokenId="'+tokenId+'"]', doc));
		
		if (tag) {
			const isVisible = tag.isVisible(true); // true to check if parents are visible
			if (!isVisible) {
				// if the tag has a note parent then reset the tokenId to that of the note
				// TODO show note
				
				const noteParent = tag.parent('note'); // TODO use note selector
				if (noteParent != null) {
					tag = noteParent.prev('dtocNoteNumber');
				}
			}

			tag.scrollIntoView(doc);

			let color = this._app.colors.index.dark;
			const type = tag.getAttribute('dtocHighlight');
			if (type === 'tag') {
				color = this._app.colors.tag.dark;
			} else if (type === 'kwic') {
				color = this._app.colors.kwic.dark;
			} else {
				console.warn('Reader.scrollToToken: no dtocHighlight attribute?', tag);
			}
			tag.frame(color, 1, {duration: 1000});
		}
	}

	/**
	 * Gets the XML document in the reader view
	 * @returns {XMLDocument}
	 */
	getReaderDoc() {
		return this.view.readerContainer.getDoc();
	}

	getStylesheet(cssUrl) {
		cssUrl = cssUrl.replace(/^..\//, ''); // handling for relative url
		const doc = this.getReaderDoc();
		for (let i = 0; i < doc.styleSheets.length; i++) {
			const ss = doc.styleSheets.item(i);
			if (ss.href.indexOf(cssUrl) !== -1) {
				return ss;
			}
		}
		return undefined;
	}

	addStylesheetToReader(cssUrl) {
		return new Promise((resolve, reject) => {
			const doc = this.getReaderDoc();
			const cssPi = doc.createProcessingInstruction('xml-stylesheet', 'href="'+cssUrl+'" type="text/css"');
			doc.insertBefore(cssPi, doc.firstChild);

			function checkForStylesheet(checkCount) {
				const added = this.getStylesheet(cssUrl) !== undefined;
				checkCount++;
				if (added) {
					if (this._app.isDebug) console.log('added', cssUrl, 'after', checkCount);
					resolve();
				} else {
					if (checkCount > 50) {
						reject();
					} else {
						setTimeout(checkForStylesheet.bind(this, checkCount), 100);
					}
				}
			}

			checkForStylesheet.call(this, 0);
		});
	}

	addRuleForElement(el, cssRule, cssStylesheet) {
		if (cssStylesheet === undefined) {
			cssStylesheet = this.getStylesheet(this.CUSTOM_CSS_URL);
		}

		let id = el.getAttribute('id');
		if (!id) {
			id = Ext.id(null, 'dtc-gen');
			el.setAttribute('id', id);
		}
		cssStylesheet.insertRule('#'+id+' { '+cssRule+'}');
	}

	hideAllNotes(destroy) {
		for (const id in this.tokenToolTipsMap) {
			this.tokenToolTipsMap[id].hide();
			if (destroy === true) {
				this.tokenToolTipsMap[id].destroy();
			}
		}
	}

	_processNotes() {
		this.hideAllNotes(true);
		this.tokenToolTipsMap = {};
		
		const customSs = this.getStylesheet(this.CUSTOM_CSS_URL);
		
		const doc = this.getReaderDoc();
		let notes = [];
		this.noteSelectors.forEach(function(selector) {
			try {
				notes = notes.concat(DocLoader.evaluateXPath(selector, doc));
			} catch(e) {
				console.error(e.message);
			}
		});
		notes.forEach(function(note, index) {
			this.addRuleForElement(note, 'display: none !important;', customSs);

			const noteNumber = doc.createElement('dtocNoteNumber');
			noteNumber.innerHTML = (index+1);
			note.parentNode.insertBefore(noteNumber, note);
			const tip = new Ext.ux.DToCToolTip(Ext.apply({
				target: Ext.get(noteNumber),
				title: 'Note '+(index+1),
				html: note.innerHTML,
				listeners: {
					beforeshow: this.hideAllNotes,
					show: function(cmp, evt) {
						const parent = cmp.getEl().dom.querySelector('.x-autocontainer-innerCt');
						this._processLinks(parent);
					},
					scope: this
				}
			}, this.view.toolTipConfig));
			
			const tokenId = note.getAttribute('tokenId');
			this.tokenToolTipsMap[tokenId] = tip;

			note.classList.add('hide');
		}, this);
	}
	
	_processImages() {
		const doc = this.getReaderDoc();
		const customSs = this.getStylesheet(this.CUSTOM_CSS_URL);

		// assign ids to graphic tags and use that for loading image urls through the content/url combo
		const images = [];
		this.imageSelectors.forEach(function(selector) {
			const att = this._getAttributeFromSelector(selector);
			if (att !== null) {
				try {
					const hits = DocLoader.evaluateXPath(selector, doc);
					hits.forEach(function(el) {
						images.push({el: el, att: att});
					});
				} catch (e) {
					console.error(e.message);
				}
			}
		}, this);
		images.forEach(function(image) {
			const url = image.el.getAttribute(image.att);
			this.addRuleForElement(image.el, 'content: url('+url+');', customSs);
		}, this);
	}
	
	_processLinks(parent) {
		const customSs = this.getStylesheet(this.CUSTOM_CSS_URL);

		const links = [];
		this.linkSelectors.forEach(function(selector) {
			const att = this._getAttributeFromSelector(selector);
			if (att !== null) {
				try {
					const hits = DocLoader.evaluateXPath(selector, parent);
					hits.forEach(function(el) {
						links.push({el: el, att: att});
					});
				} catch (e) {
					console.error(e.message);
				}
			}
		}, this);
		links.forEach(function(link) {
			link.el.setAttribute('dtocLink', '');

			const url = link.el.getAttribute(link.att);
			if (url.indexOf("http://")===0 || url.indexOf("https://")===0) {
				link.el.addEventListener('click', function(e) {
					window.open(url);
				}, link);
			}
		}, this);
	}
	
	_getAttributeFromSelector(selector) {
		let attName = selector.match(/(@)(\w*)/);
		if (attName !== null) {
			attName = attName[2];
		}
		return attName;
	}

	_highlightDocSelections() {
		const sels = this._app.docmodel.getSelectionsForDoc(this.docId);
		for (const type in sels) {
			const tokenIds = sels[type];
			for (const id in tokenIds) {
				this.highlightTag({docId: this.docId, tokenId: id, type: type});
			}
		}
	}
	
	/**
	 * A general highlight function.
	 * Need to specify docId and highlight type, and one of: id, tokenId, tag, xpath.
	 * @param {Object} data
	 * @param {String} data.docId
	 * @param {String} data.type
	 * @param {String} [data.id]
	 * @param {String} [data.tokenId]
	 * @param {String} [data.tag]
	 * @param {String} [data.xpath]
	 */
	highlightTag(data) {
		if (data.docId === this.docId) {
			let tag = null;
			const doc = this.getReaderDoc();
			if (data.id) {
				tag = doc.querySelector('*[xml\\:id="'+data.id+'"]');
			} else if (data.tokenId) {
				tag = doc.querySelector('*[tokenId="'+data.tokenId+'"]');
			} else if (data.tag) {
				tag = doc.querySelectorAll(data.tag)[data.index];
			} else if (data.xpath) {
				throw new Error('Reader.highlightTag needs xpath support');
			} 
			if (tag !== null) {
				tag.setAttribute('dtocHighlight', data.type);
			}
		}
	}
	
	clearHighlights(type) {
		this.getReaderDoc().querySelectorAll('[dtocHighlight]').forEach(hit => {
			if (type === undefined || hit.getAttribute('dtocHighlight') === type) {
				hit.removeAttribute('dtocHighlight');
			}
		});
	}

}

