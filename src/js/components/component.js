export default class Component {
    view = undefined;
    constructor(app) {
        this._app = app;
    }

    subscribe(event, handler, scope = undefined) {
        if (scope !== undefined) {
            if (this._app.isDebug) console.debug('subscribe binding handler:', event, handler);
            handler = handler.bind(scope); // legacy handling
        }
        return this._app.subscribe(event, handler);
    }

    unsubscribe(handler) {
        this._app.unsubscribe(handler);
    }

    publish(event, data) {
        this._app.publish(event, data);
    }

    getCorpus() {
        return this._app.getCorpus();
    }

}