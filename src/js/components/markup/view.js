import DToCPanel from '../../app/panel.js';

export default Ext.define('MarkupView', {
	extend: 'Ext.grid.Panel',
	mixins: ['DToC.Panel'],

	_maskEl: null,

	currentChapterFilter: null,

	constructor: function(config) {
		this._controller = config._controller;
		
		this.callParent(arguments);
		this.mixins['DToC.Panel'].constructor.apply(this, arguments);
	},

	initComponent: function() {
		var me = this;
		
		var tagStore = Ext.create('Ext.data.JsonStore', {
			fields: [
				{name: 'xpath', allowBlank: false},
				{name: 'label', allowBlank: false},
				{name: 'freq', type: 'int'},
				{name: 'usage'}
			],
			sortInfo: {field: 'label', direction: 'ASC'}
		});
		
		Ext.apply(me, {
			title: 'Tags',
			store : tagStore,
			deferRowRender: true,
			viewConfig: {
				markDirty: false
			},
			selModel: Ext.create('Ext.selection.RowModel', {
				mode: 'MULTI',
				listeners: {
					selectionchange: {
						fn: function(sm, selections) {
							this._controller._app.view.showMultiSelectMsg(this);
							
							this.handleSelections(selections);
						},
						scope: this
					}
				}
			}),
			dockedItems: [{
				dock: 'top',
				xtype: 'toolbar',
				items: [{
					xtype: 'textfield',
					itemId: 'filter',
					emptyText: 'Filter',
					width: 120,
					enableKeyEvents: true,
					listeners: {
						keyup: this.textFilterHandler,
						scope: this
					}
				}, '->', {
					text: 'Filter by Chapter',
					itemId: 'markupChapterFilter',
					menu: {
						items: [],
						plain: true,
						showSeparator: false,
						listeners: {
							click:  function(menu, item) {
								this.doChapterFilter(item.initialConfig.docId, true);
							},
							scope: this
						}
					}
				}]
			}],
			hideHeaders: true,
			columns: [
				{header: 'Label', dataIndex: 'label', flex: 1},
				{header: 'Freq', dataIndex: 'freq', align: 'right'}
			]
		});
		
		me.callParent(arguments);
	},

	handleSelections: function(selections) {
		var docIdFilter;
		this.down('#markupChapterFilter').getMenu().items.each(function(item) {
			if (item.checked) {
				docIdFilter = item.initialConfig.docId;
				return false;
			}
			return true;
		});
		var tags = [];
		for (var i = 0; i < selections.length; i++) {
			var sel = selections[i].data;
			if (docIdFilter !== undefined) {
				var tagData = this._controller.tagMetadata[docIdFilter][sel.xpath];
				if (tagData != null) {
					tags.push(tagData);
				}
			} else {
				for (var docId in this._controller.tagMetadata) {
					var tagData = this._controller.tagMetadata[docId][sel.xpath];
					if (tagData != null) {
						tags.push(tagData);
					}
				}
			}
		}
		this._controller.publish('tagsSelected', tags);
	},

	clearSelections: function() {
		this.getSelectionModel().deselectAll(true);
	},

	updateChapterFilter: function(corpus) {
		// OLD TODO take possible curation into account
		var menu = this.down('#markupChapterFilter').getMenu();
		menu.removeAll();
		
		corpus.documents.forEach((doc) => {
			// check to see if this doc was specified as the index (via the cwrc interface)
			if (doc.isDtocIndex !== true) {
				menu.add({
					xtype: 'menucheckitem',
					docId: doc.id,
					group: 'markupchapters',
					text: doc.getShortTitle()
				});
			}
		}, this);
	},
	
	textFilterHandler: function() {
		var value = this.down('#filter').getValue();
		if (value == '') {
			this.getStore().clearFilter();
		} else {
			var regex = new RegExp('.*'+value+'.*', 'i');
			this.getStore().filterBy(function(r) {
				return r.get('label').match(regex) !== null;
			});
		}
	},
	
	doChapterFilter: function(docId, local) {
		if (docId === this.currentChapterFilter) {
			docId = null;
		}
		
		var menuItem;
		this.down('#markupChapterFilter').getMenu().items.each(function(item) {
			if (item.initialConfig.docId === docId) {
				menuItem = item;
			} else {
				item.setChecked(false);
			}
		}, this);
		
		var textFilter = this.down('#filter').getValue();
		var regex = new RegExp('.*'+textFilter+'.*', 'i');
		
		if (docId === null) {
			this.currentChapterFilter = null;
			this.getStore().clearFilter();
			this.getStore().each(function(record) {
				var freqTotal = this._controller.tagTotals[record.get('xpath')].freq;
				record.set('freq', freqTotal);
			}, this);
			if (textFilter != '') {
				this.textFilterHandler();
			}
			docId = null;
		} else {
			menuItem.setChecked(true);
			this.currentChapterFilter = docId;
			var docTags = this._controller.tagMetadata[docId];
			this.getStore().clearFilter();
			this.getStore().filterBy(function(record, id) {
				var xpath = record.get('xpath');
				var tagObj = docTags[xpath];
				if (tagObj != undefined && tagObj != null) {
					record.set('freq', tagObj.length);
					if (textFilter != '') {
						return record.get('label').match(regex) !== null;
					}
					return true;
				}
				return false;
			}, this);
		}
		if (local) {
			this._controller.publish('chapterFilterSelected', docId);
		}
	}
});
