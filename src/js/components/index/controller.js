import Component from '../component.js';
import IndexView from './view.js';
import MarkupProcessor from '../markup/processor.js';

/**
 * Processes a TEI index document with the following sort of structure:
 * <text><div><list>
 * 		<item xml:id="ie494">
 *			<ref target="#p992">Irvine, Dean</ref>
 *			<list>
 *				<item xml:id="ie495">
 *					<ref target="#p91 #p92 #p93">on a modernist commons</ref>
 *				</item>
 *			</list>
 *		</item>
 *	</list></div></text>
 */
export default class Index extends Component {
	/**
	 * A JSON version of the index XML
	 * @typedef {Object} Index~indexJson
	 * @prop {String} text The title text for the index entry including the count
	 * @prop {Number} count The number of targets for the index entry
	 * @prop {Number} textNoCount The title text for the index entry not including the count
	 * @prop {Array} targets The array of target IDs for the index entry
	 * @prop {String} indexId The index ID for this entry
	 * @prop {Boolean} isCrossRef True if the entry is a cross reference
	 * @prop {Boolean} leaf True if the entry has no children
	 * @prop {Array} [children] An array of child objects of this entry
	 * 
	 */
	indexJson = undefined;

	/**
	 * Cached data about the actual index targets. Each map key is a target ID.
	 * @typedef {Object} Index~indexTargetData
	 * @prop {String} docId The ID of the document where the target is found
	 * @prop {String} tokenId The tokenId of the tag that's targeted
	 * @prop {String} tag The tag name
	 * @prop {String} text The text content of the tag
	 * @prop {String} [prevText] Some text preceding the target
	 * @prop {String} [nextText] Some text following the target
	 */
	indexTargetData = {};

	/**
	 * A string that prefixes the ID of each index entry.
	 * Used to identify cross references.
	 */
	indexIdPrefix = 'ie';

	noTargetIds = []; // track the index items that have no target IDs specified
	noMatchingTargetIds = []; // track the index items that have no matches in the documents

	constructor(app) {
		super(app);
		this.view = new IndexView({_controller: this});

		this.subTokens = [];
		this.subTokens.push(this.subscribe('loadedCorpus', this.handleLoadedCorpus.bind(this)));
		this.subTokens.push(this.subscribe('allTagsLoaded', this.handleAllTagsLoaded.bind(this)));
		this.subTokens.push(this.subscribe('chapterFilterSelected', this.handleChapterFilterSelected.bind(this)));
		this.subTokens.push(this.subscribe('clearSelections', this.handleClearSelections.bind(this)));
	}

	handleLoadedCorpus(src, corpus) {
		this.view.updateChapterFilter();
	}

	handleAllTagsLoaded(src) {
		if (this.view.rendered) {
			this.view.getView().refresh();
			this.view.body.unmask();
		}
	}

	handleChapterFilterSelected(src, docId) {
		this.view.doChapterFilter(docId, false);
	}

	handleClearSelections(src, data) {
		this.view.clearSelections();
	}

	remove() {
		this.subTokens.forEach(token => this.unsubscribe(token), this);
		
		this.view.remove();
	}	

	async loadIndex(indexDoc) {
		this.indexJson = await this._getIndexJsonFromXml(indexDoc);

		if (this.indexJson !== undefined) {
			this.view.getRootNode().appendChild(this.indexJson.children);
			// this.view.getStore().sort('text', 'ASC');
			
			if (this.noTargetIds.length > 0 || this.noMatchingTargetIds.length > 0) {
				const topdock = this.view.getDockedComponent('topToolbar');
				const button = topdock.insert(3, {
					xtype: 'button',
					glyph: 'xf071@FontAwesome',
					padding: '3 5 3 3',
					tooltip: 'Index Issues',
					handler: function(btn) {
						let msg = '';
						if (this.noTargetIds.length > 0) {
							msg += '<p>The following index entries had no target IDs:<br><code>'+this.noTargetIds.join(', ')+'</code></p>';
						}
						if (this.noMatchingTargetIds.length > 0) {
							msg += '<p>The following target IDs had no matching document tags:<br><code>'+this.noMatchingTargetIds.join(', ')+'</code></p>';
						}
						this._app.showErrorMsg('Index Issues', msg);
						topdock.remove(button);
					},
					scope: this
				});
			}

			this.publish('indexProcessed', this);
		}
	}

	/**
	 * Takes the index document and returns a processed JSON version
	 * @param {Document} indexDoc 
	 * @returns {Object}
	 */
	async _getIndexJsonFromXml(indexDoc) {
		const items = indexDoc.querySelectorAll('div > list > item');
		if (items.length == 0) {
			this._app.showErrorMsg({title: 'DToC Indexer', message:'No index items were found!'});
			this.view.body.unmask();
			return undefined;
		} else {
			const rootConfig = {
				text: 'Root',
				children: []
			};
			this.noTargetIds = this._processIndex(items, rootConfig);
			this.noMatchingTargetIds = await this._populateIndexTargetData(rootConfig);

			return rootConfig;
		}
	}

	/**
	 * 
	 * @param {Array} items An array of Elements
	 * @param {Object} parentConfig The parent JSON object within which to put the processed items
	 */
	_processIndex(items, parentConfig) {
		function getTitle(el) {
			var title = undefined;
			var children = el.childNodes;
			for (var i = 0; i < children.length; i++) {
				var child = children[i];
				if (child.nodeType === Node.ELEMENT_NODE) {
					var name = child.nodeName.toLowerCase();
					if (name !== 'item' && name !== 'list') {
						title = child.textContent;
					}
				}
			}
			if (title === undefined) {
				title = '';
				var next = el.firstChild;
				while (next.nodeType === Node.TEXT_NODE) {
					title += next.textContent;
					next = next.nextSibling;
				}
			}
			title = title.replace(/\s+/g, ' ');
			title = title.trim();
			return title;
		}
		
		// find a descendant ref and ensure that there aren't other item tags between it and the original tag
		function doesRefDescendFromItem(extEl) {
			var ref = extEl.child('ref');
			var isDescendant = true;
			if (ref != null) {
				var parent = ref.parent();
				while (parent != extEl) {
					if (parent.nodeName.toLowerCase() === 'item') {
						isDescendant = false;
						break;
					}
					parent = parent.parent();
				}
			} else {
				isDescendant = false;
			}
			return isDescendant;
		}
		
		const noTargetIds = [];

		for (var i = 0; i < items.length; i++) {
			var item = items[i];
			var indexId = item.getAttribute('id') || item.getAttribute('xml:id');
			var text = undefined;
			var title = undefined;
			var count = 0;
			var targetIds = [];
			var subItems = [];
			var childConfig = undefined;
			var isCrossRef = false;
			
			const ref = item.querySelector('ref');
			if (ref) {
				text = getTitle(item);
				var target = ref.getAttribute('target');
				if (target !== null) {
					var ids = target.split(/\s+/);
					for (var j = 0; j < ids.length; j++) {
						var id = ids[j].replace(/^#/, '');
						if (id !== '') {
							if (id.match(/^[a-z]/i) !== null) { // must start with a letter
								if (id.indexOf(this.indexIdPrefix) === 0) {
									isCrossRef = true;
								}
								targetIds.push(id);
								this.indexTargetData[id] = true; // set to true for now, we'll replace with hit data later
								count++;
							}
						}
					}
				}
				title = text + ' ('+count+')';
				parentConfig.count += count;
//              parentNode.setText(parentNode.getData().textNoCount + ' ('+parentNode.getData().count+')');
			}
			
			if (item.querySelector('list')) {
				if (text === undefined) {
					text = getTitle(item);
					title = text;
				}
				subItems = item.querySelectorAll('list > item');
			}
			
			var isLeaf = subItems.length == 0;
			
			if (text === undefined || title === undefined) {
				console.log('no text',indexId);
//			    text = title = '';
//			    var doesIt = doesRefDescendFromItem(item);
//			    console.log(doesIt);
//			    console.log('---');
//			    console.log(item.innerHTML);
//			    console.log('=============');
			} else {
				if (targetIds.length === 0) {
					noTargetIds.push(indexId);
				}

				childConfig = {
					cls: 'index',
					text: title,
					count: count,
					textNoCount: text,
					targets: targetIds,
					leaf: isLeaf,
					indexId: indexId,
					isCrossRef: isCrossRef
				};
				
				parentConfig.children.push(childConfig);
				if (!isLeaf) {
					childConfig.children = [];
					this._processIndex(subItems, childConfig);
				}
			}
		}

		return noTargetIds;
	}

	async _populateIndexTargetData(rootConfig) {
		function doPopulate(doc) {
			return new Promise(resolve => {
				for (let indexId in this.indexTargetData) {
					if (this.indexTargetData[indexId] !== true) continue; // already found
					let hit;
					try {
						hit = doc.xml.querySelectorAll('*[*|id="'+indexId+'"]').item(0);
					} catch (e) {
						if (window.console) {
							console.warn('bad ID', indexId);
						}
					}
					if (hit) {
						const text = MarkupProcessor.getText(hit);
						let hitdata = {
							docId: doc.id,
							tokenId: hit.getAttribute('tokenId'),
							tag: hit.nodeName,
							text: text.content
						}
						
						if (text.shortened === false) {
							const surrText = MarkupProcessor.getSurroundingText(hit);
							hitdata.prevText = surrText[0];
							hitdata.nextText = surrText[1];
						} else {
							hitdata.text += '&hellip;';
						}
						this.indexTargetData[indexId] = hitdata;
					}
				}
				setTimeout(() => {
					resolve();
				}, 0);
			});
		}

		const docs = this.getCorpus().documents;
		for (let i = 0; i < docs.length; i++) {
			const doc = docs[i];
			if (doc.isDtocIndex) continue;
			this.publish('populatingIndex', doc);
			await doPopulate.call(this, doc);
		}

		const noMatchingTargetIds = [];
		for (let indexId in this.indexTargetData) {
			if (this.indexTargetData[indexId] === true) {
				noMatchingTargetIds.push(indexId);
				delete this.indexTargetData[indexId]; // remove ids that don't have hit data
			}
		}

		function setTargetMatches(config) {
			if (config.targets) {
				const match = config.targets.some((targetId) => {
					return this.indexTargetData[targetId] !== undefined;
				}, this);
				if (match) {
					config.targetMatches = true;
				}
			}
			if (config.children) {
				config.children.forEach((child) => {
					setTargetMatches.call(this, child);
				}, this);
			}
		}
		setTargetMatches.call(this, rootConfig);

		return noMatchingTargetIds;
	}
}
