import PubSub from 'pubsub-js';
import DocLoader from './app/docloader.js';
import Searcher from './app/searcher.js';
import Markup from './components/markup/controller.js';
import Index from './components/index/controller.js';
import Reader from './components/reader/controller.js';
import ToC from './components/toc/controller.js';
import DocModel from './components/docmodel/controller.js';
import DToCAppView from './app/view.js'

export default class DToCApp {
	colors = {
		index: {
			dark: '#F47922',
			light: '#FBD7B2'
		},
		tag: {
			dark: '#249EF5',
			light: '#B1D6FB'
		},
		kwic: {
			dark: '#E324F5',
			light: '#FBB1FB'
		}
	}

	_corpus = undefined;
	getCorpus() {
		return this._corpus;
	}
	setCorpus(corpus) {
		this._corpus = corpus;
	}

	_apiParams = {};
	getApiParam(key) {
		return this._apiParams[key];
	}
	setApiParam(key, value) {
		this._apiParams[key] = value;
	}

	_urlBase = window.location.toString().endsWith('/') ? window.location.toString() : window.location.toString()+'/';
	get urlBase() {
		return this._urlBase;
	}
	set urlBase(urlBase) {
		this._urlBase = urlBase;
	}

	_version = '0.3.1'; // TODO set this programmatically
	get version() {
		return this._version;
	}

	_useIndex = false;
	get useIndex() {
		return this._useIndex;
	}
	set useIndex(useIndex) {
		this._useIndex = useIndex;
	}

	_isCurator = false;
	get isCurator() {
		return this._isCurator;
	}
	set isCurator(isCurator) {
		this._isCurator = isCurator;
	}

	_isDebug = false;
	get isDebug() {
		return this._isDebug;
	}
	set isDebug(isDebug) {
		this._isDebug = isDebug;
	}

	/**
	 * The input configuration used to load documents
	 */
	_inputConfig = undefined;
	getInputConfig() {
		const config = Object.assign({}, this._inputConfig);
		
		const tagData = this.markup.getExportData();
		const tocData = this.toc.getExportData();
		config.curation = {
			markup: tagData,
			toc: tocData
		}
		
		delete config.callback;

		return config;
	}
	setInputConfig(inputConfig) {
		this._inputConfig = inputConfig;
	}

	constructor(config) {
		if (config && config.dtocRootUrl !== undefined) {
			this.urlBase = config.dtocRootUrl;
		}
		if (config && config.debug) {
			this.isDebug = config.debug;
		}

		this.markup = new Markup(this);
		this.reader = new Reader(this, {showMenu: config.showMenu});
		this.toc = new ToC(this);
		this.docmodel = new DocModel(this);
		this.searcher = new Searcher(this);

		if (Ext === undefined) {
			throw new Error('The ExtJS library could not be found or did not initialize');
		}
		Ext.onReady(function() {
			this.view = new DToCAppView({_controller: this});
			if (config && config.input) {
				this.load(config.input);
			}
		}, this);
	}

	subscribe(event, handler) {
		return PubSub.subscribe(event, handler);
	}

	unsubscribe(tokenOrHandler) {
		PubSub.unsubscribe(tokenOrHandler);
	}

	publish(event, data) {
		if (this.isDebug) console.log(event);
		PubSub.publish(event, data);
	}

	showInputWindow(data) {
		if (this.view && this.view.getViewport().rendered) {
			this.view.showInputWindow(data);
		} else {
			this.subscribe('viewportReady', function() {
				this.view.showInputWindow(data);
			}.bind(this));
		}
	}

	search(query, docId) {
		return this.searcher.search(query, docId);
	}

	reset() {
		this.markup.reset();
		this.reader.reset();
		this.toc.reset();
		this.docmodel.reset();
		this.searcher.reset();
	}

	load(config) {
		if (this.view === undefined || this.view.getViewport().rendered === false) {
			if (this.isDebug) console.log('waiting to load');
			this.subscribe('viewportReady', function() {
				this.load(config);
			}.bind(this));

			return;
		}

		this.setInputConfig(config);

		config.callback = function(status) {
			if (status.error) {
				this.view.loadMask.close();
				this.showInputWindow(this._inputConfig);
				this.showErrorMsg('Error loading document(s)', status.error);
			} else if (status.corpus) {
				if (this.getCorpus() !== undefined) {
					this.reset();
				}

				this.setCorpus(status.corpus);

				const docsToIndex = Array.from(status.corpus.documents.values()).filter((doc) => doc.isDtocIndex === false);

				this.subscribe('indexingDocument', (event, data) => {
					const index = this.getCorpus().getDocumentIndex(data);
					const value = index / docsToIndex.length;
					this.view.loadMask.update('Indexing Documents', value);
				});
				this.subscribe('processingDocument', (event, data) => {
					const index = this.getCorpus().getDocumentIndex(data);
					const value = index / docsToIndex.length;
					this.view.loadMask.update('Processing Documents', value);
				});
				this.subscribe('populatingIndex', (event, data) => {
					const index = this.getCorpus().getDocumentIndex(data);
					const value = index / docsToIndex.length;
					this.view.loadMask.update('Building Index', value);
				});

				setTimeout(async function() {
					try {
						const corpus = this.getCorpus();

						if (this.isDebug) console.time('indexing');
						await this.searcher.indexDocuments(docsToIndex);
						if (this.isDebug) console.timeEnd('indexing');

						const indexDoc = Array.from(corpus.documents.values()).find((doc) => doc.isDtocIndex);
						if (indexDoc !== undefined) {
							this.useIndex = true;
							this.index = new Index(this);
							this.view.showIndex();
							this.setApiParam('indexDocId', indexDoc.id);
							await this.index.loadIndex(indexDoc.xml);
						}

						this.view.loadMask.animate({
							duration: 500,
							to: { opacity: 0 },
							listeners: {
								afteranimate: function() {
									this.view.loadMask.close();
								},
								scope: this
							}
						});

						// handle curation
						let curation = {
							markup: [],
							toc: []
						};
						if (config.curation !== undefined) {
							if (typeof config.curation === 'string') {
								try {
									curation = JSON.parse(config.curation);
								} catch (e) {
									console.warn('error parsing curation', config.curation);
								}
							} else {
								curation = config.curation;
							}
						}
						// copy curation paths to curation object
						if (config.documentImages !== undefined && config.documentImages !== '') {
							curation.markup.push({
								xpath: config.documentImages,
								label: 'graphic',
								usage: 'image'
							});
						}
						if (config.documentNotes !== undefined && config.documentNotes !== '') {
							curation.markup.push({
								xpath: config.documentNotes,
								label: 'note',
								usage: 'note'
							});
						}
						if (config.documentLinks !== undefined && config.documentLinks !== '') {
							curation.markup.push({
								xpath: config.documentLinks,
								label: 'link',
								usage: 'link'
							});
						}
				
						if (curation.markup && curation.markup.length > 0) {
							for (let i = 0; i < curation.markup.length; i++) {
								const tag = curation.markup[i];
								this.markup.curatedTags[tag.xpath] = tag;
								if (tag.usage) {
									if (tag.usage === 'link') {
										this.reader.linkSelectors.push(tag.xpath);
									} else if (tag.usage === 'note') {
										this.reader.noteSelectors.push(tag.xpath);
									} else if (tag.usage === 'image') {
										this.reader.imageSelectors.push(tag.xpath);
									}
								}
							}
						}
						
						if (curation.toc !== undefined && curation.toc.length > 0) {
							corpus.setDocumentsOrder(curation.toc);
						}


						this.publish('loadedCorpus', corpus);

						if (this.isDebug) console.timeEnd('load');

						let docId = this.getApiParam('docId');
						if (docId === undefined) {
							docId = corpus.getDocument(0).id;
							if (this.useIndex) {
								var indexDocId = this.getApiParam('indexDocId');
								const firstDoc = corpus.documents.find((doc) => doc.id !== indexDocId);
								if (firstDoc) {
									docId = firstDoc.id;
								}
							}
						}
						this.publish('documentSelected', {docId});
					} catch (err) {
						this.view.showErrorMsg('Error loading document(s)', 'Error indexing documents');
					}
				}.bind(this), 0);
			} else {
				this.view.loadMask.update(status.msg, 0);
			}
		}.bind(this);
		if (this.isDebug) console.time('load');
		this.view.loadMask.show();
		try {
			DocLoader.loadCorpusFromInputs(config);
		} catch (e) {
			this.view.showErrorMsg('Error loading document(s)', e);
		}
	}

	showConfig() {
		const config = this.getInputConfig();
		this.view.showExportMsg(JSON.stringify(config, null, 2));
	}

	showDefaultView() {
		this.isCurator = false;
		
		this.markup.showDefaultView();
		this.toc.showDefaultView();

		this.view.getViewport().updateLayout();
	}

	showEditView() {
		this.isCurator = true;

		this.markup.showEditView();
		this.toc.showEditView();

		this.view.getViewport().updateLayout();
	}
	
	showHelp() {
		this.view.openUrl("https://cwrc.ca/Documentation/public/DITA_Files-Various_Applications/DToC/OverviewDToC.html");
	}

	showErrorMsg(title, msg) {
		this.view.showErrorMsg(title, msg);
	}
}
