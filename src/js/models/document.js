export default class Document {
	/**
	 * The Document class
	 * @param {Object} config
	 * @param {String} config.id
	 * @param {String} config.title
	 * @param {String} config.author
	 * @param {Node} config.xml
	 * @param {String} [config.css]
	 * @param {Boolean} [config.isDtocIndex]
	 */
	constructor({id, title, author, xml, css = '../css/xml/default.css', isDtocIndex = false}) {
		this._id = id;
		this._title = title;
		this._author = author;
		this.xml = xml;
		
		this._css = css;
		this._isDtocIndex = isDtocIndex;
		
		this._tokensCount = undefined;
		this._termsCount = undefined;
	}

	get id() {
		return this._id;
	}
	set id(id) {
		this._id = id;
	}

	get index() {
		throw new Error('No Index!');
	}
	set index(index) {
		throw new Error('No Index!');
	}

	get origIndex() {
		throw new Error('No Index!');
	}
	set origIndex(origIndex) {
		throw new Error('No Index!');
	}

	get title() {
		return this._title;
	}
	set title(title) {
		this._title = title;
	}

	getShortTitle() {
		return this.getTruncated(this._title, 25);
	}

	getHtmlTitle() {
		let titleHtml = '';
		
		const title = this.title.normalize();
		
		let names = '';
		let colon = ': ';
		let authors = this.author;
		if (authors !== undefined) {
			if (typeof authors === 'string') {
				authors = [authors];
			}
			for (var i = 0; i < authors.length; i++) {
				if (i > 0) {
					if (authors.length > 2) names += ', ';
					if (i == authors.length - 1) {
						names += ' and ';
					}
				}
				names += authors[i];
			}
			if (names === '') {
				// no authors so get rid of colon
				colon = '';
			}
			titleHtml += '<span class="author">'+names+'</span>'+colon;
		}
		titleHtml += '<span class="title">'+title+'</span>';

		return titleHtml;
	}
	
	get author() {
		return this._author;
	}
	set author(author) {
		this._author = author;
	}

	get xml() {
		return this._xml;
	}
	set xml(xml) {
		this._xml = xml;
		this._xmlString = new XMLSerializer().serializeToString(this._xml)
			.replace(/\s+/g, ' '); // consolidate all whitespace
	}

	get xmlString() {
		return this._xmlString;
	}

	get css() {
		return this._css;
	}
	set css(css) {
		this._css = css;
	}

	get tokensCount() {
		return this._tokensCount;
	}
	set tokensCount(tokensCount) {
		this._tokensCount = tokensCount;
	}

	get termsCount() {
		return this._termsCount;
	}
	set termsCount(termsCount) {
		this._termsCount = termsCount;
	}

	get isDtocIndex() {
		return this._isDtocIndex;
	}
	set isDtocIndex(isDtocIndex) {
		this._isDtocIndex = isDtocIndex;
	}

	getTruncated(string, max) {
		if (string.length > max) {
			// maybe a file or URL?
			var slash = string.lastIndexOf("/");
			if (slash > -1) {
				string = string.substr(slash+1);
			}

			if (string.length > max) {
				var space = string.indexOf(" ", max-5);
				if (space < 0 || space > max) {
					space = max;
				}
				string = string.substring(0, space) + "…";
			}
		}
		return string;  
	}
}
