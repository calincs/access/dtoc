import DTOCInputDialog from './input.dialog.js';

export default Ext.define('DToCAppView', {
	extend: 'Ext.app.Application',
	name: 'DToCApp',

	settingsWin: null,
	inputWindow: null,
	loadMask: null,

	doShowMultiSelectMsg: true, // let the user know they can select multiple items

	constructor: function(config) {
		this._controller = config._controller;

		this.callParent(arguments);

		this.loadMask = new Ext.window.Window({
			title: 'Loading',
			width: 350,
			height: 100,
			minimizable: false,
			closeAction: 'hide',
			closable: false,
			modal: true,
			layout: {
				type: 'vbox',
				align: 'stretch',
				pack: 'center',
			},
			items: [{
				itemId: 'progressBar',
				xtype: 'progressbar',
				height: 20,
				margin: 10
			}],
			update: function(text, value) {
				this.down('#progressBar').updateProgress(value, text);
			}
		});
	},

	getViewport: function() {
		return Ext.ComponentQuery.query('viewport')[0];
	},

	launch: function() {
		this.viewport = Ext.create('Ext.container.Viewport', {
			layout: 'border',
			header: false,
			items: [{
				id: 'dtcContainer',
				region: 'center',
				margin: '10 0 0 10',
				layout: {
					type: 'hbox',
					align: 'stretch'
				},
				items: [{
					title: 'Index & Tags',
					header: {
						hidden: true
					},
					id: 'dtcTools',
					width: 310,
					minWidth: 200,
					xtype: 'tabpanel',
					cls: 'dtc-panel',
					deferredRender: false,
					activeTab: 0,
					items: [],
					collapsed: false,
					animCollapse: false,
					collapseDirection: 'left',
					listeners: {
						collapse: function(p) {
							p.el.down('.x-panel-header').addCls('borderRadiusTop borderRadiusBottom');
						}
					
					}
				},{
					xtype: 'splitter',
					width: 10,
					listeners: {
						afterrender: function(splitter) {
							splitter.getEl().on('click', this.onSplitterClick.bind(this, splitter));
						},
						scope: this
					}
				},{
					xtype: 'splitter',
					width: 10,
					listeners: {
						afterrender: function(splitter) {
							splitter.getEl().on('click', this.onSplitterClick.bind(this, splitter));
						},
						scope: this
					}
				},{
					flex: 1,
					minWidth: 350,
					margin: '0 10 0 0',
					id: 'dtcReaderParent',
					layout: 'hbox',
					layoutConfig: {
						pack: 'start',
						align: 'stretch'
					},
					collapsible: false,
					items: []
				}],
				listeners: {
					boxready: function() {
						setTimeout(function() {
							this._controller.publish('viewportReady', this);
						}.bind(this), 0);
					},
					scope: this
				}
			},{
				region: 'south',
				height: 20,
				margin: '0 10 0 10',
				html: '<div id="dtcFooter"><a href="https://gitlab.com/calincs/cwrc/dtoc/dtoc-2.0/-/issues/new" target="_blank">Bug / Request</a> | DToC v.'+this._controller.version+'</div>'
			}]
		});
		
		this.callParent(arguments);

		Ext.getCmp('dtcContainer').insert(2, this._controller.toc.view);

		Ext.getCmp('dtcTools').insert(1, this._controller.markup.view);

		Ext.getCmp('dtcTools').setActiveTab(0);

		Ext.getCmp('dtcReaderParent').insert(0, this._controller.docmodel.view);
		Ext.getCmp('dtcReaderParent').insert(1, this._controller.reader.view);
		
		Ext.getCmp('dtcContainer').updateLayout();
	},

	showIndex: function() {
		Ext.getCmp('dtcTools').insert(0, this._controller.index.view);
		Ext.getCmp('dtcTools').setActiveTab(0);
	},

	showInputWindow: function(data) {
		if (this.inputWindow === null) {
			this.inputWindow = Ext.create('DToC.InputDialog', {
				inputCallback: this._controller.load.bind(this._controller)
			});
		}

		this.inputWindow.show();
		if (data) {
			this.inputWindow.populateForm(data);
		}
	},

	openUrl: function(url) {
		var win = window.open(url);
		if (!win) { // popup blocked
			Ext.Msg.show({
				title: "Popup Blocked",
				buttonText: {ok: "Close"},
				icon: Ext.MessageBox.INFO,
				message: "A popup window was blocked. <a href='"+url+"' target='_blank' class='link'>Click here</a> to open the new window.",
				buttons: Ext.Msg.OK
			});
		}
	},

	showExportMsg: function(exportData) {
		console.log(exportData)
		Ext.create('Ext.window.Window', {
			title: 'Configuration',
			width: 600,
			height: 330,
			modal: true,
			items: [{
				xtype: 'component',
				html: '<div style="text-align: center; padding: 10px;"><textarea readonly rows="15" cols="80" style="resize: none;">'+exportData+'</textarea></div>'
			}],
			buttonAlign: 'center',
			buttons: [{
				text: 'Close',
				handler: function(btn) {
					btn.up('window').close();
				}
			}]
		}).show();
	},
	
	showMultiSelectMsg: function(panel) {
		if (this.doShowMultiSelectMsg) {
			var msg = 'You can select multiple items.';
			if (Ext.os.is.Windows) {
				msg += '<br/>Use the CTRL or Shift key and click.'
			} else if (Ext.os.is.MacOS) {
				msg += '<br/>Use the Command or Shift key and click.'
			}

			panel.toastInfo({
				html: msg,
				autoCloseDelay: 3000,
				align: 't'
			});
			this.doShowMultiSelectMsg = false;
		}
	},

	showErrorMsg: function(title, msg) {
		Ext.Msg.show({
			title: title,
			buttonText: {ok: "Close"},
			icon: Ext.MessageBox.ERROR,
			message: msg,
			buttons: Ext.Msg.OK
		});
	},
	
	onSplitterClick: function(splitter, event, el) {
		var panel = splitter.prev();
		panel.toggleCollapse();
	}
});