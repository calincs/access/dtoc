import copy from 'rollup-plugin-copy';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import terser from '@rollup/plugin-terser';
import html from '@rollup/plugin-html';

export default {
	input: 'src/js/app.js',
	output: [{
		file: 'public/dtoc.js',
		format: 'iife',
		name: 'dtoc',
		strict: false
	},{
		file: 'public/dtoc.min.js',
		format: 'iife',
		name: 'dtoc',
		strict: false,
		plugins: [terser()]
	}],
	plugins: [
		nodeResolve(),
		commonjs({
			include: /node_modules/,
			requireReturnsDefault: 'auto'
		}),
		copy({
			targets: [
				{src: 'src/lib', dest: 'public'},
				{src: 'src/data', dest: 'public'},
				{src: 'src/images', dest: 'public'},
				{src: 'src/css', dest: 'public'}
			]
		}),
		html({
			template: () => {
				return `
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>DToC</title>
		<script type="text/javascript" src="./lib/ext/ext.js"></script>
		<script type="text/javascript" src="./lib/saxon/saxon.js"></script>
		<script type="text/javascript" src="./dtoc.js"></script>
		<link rel="stylesheet" type="text/css" href="./lib/ext/ext.css" />
		<link rel="stylesheet" type="text/css" href="./css/dtc.css" />
		<script src="https://kit.fontawesome.com/891f15ee2f.js" crossorigin="anonymous"></script>
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&family=Roboto+Slab:wght@300;400&display=swap" rel="stylesheet">
	</head>
	<body>
<script type="text/javascript">
window.dtocApp = new dtoc();
</script>
	</body>
</html>
`;
			}
		})
	]
};