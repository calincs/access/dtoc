# Dynamic Table of Contexts 2.0

This project is based on [Voyant Dynamic Table of Contexts](https://voyant-tools.org/dtoc/?corpus=regenerations) and aims to be an independent and more robust iteration.

[Current build](https://calincs.gitlab.io/cwrc/dtoc/dtoc-2.0/)

## Installation

1. Get the files
	- Either add as a dependency: `npm i @cwrc/dtoc`
	- Or download: `https://registry.npmjs.org/@cwrc/dtoc/-/dtoc-0.x.x.tgz`
2. Copy files to a directory in your workspace
3. Initialize (and provide a configuration object)
```html
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript" src="./lib/ext/ext.js"></script>
		<script type="text/javascript" src="./lib/saxon/saxon.js"></script>
		<script type="module" src="./dtoc.min.js"></script>
		<link rel="stylesheet" type="text/css" href="./lib/ext/ext.css" />
		<link rel="stylesheet" type="text/css" href="./css/dtc.css" />
		<script src="https://kit.fontawesome.com/891f15ee2f.js" crossorigin="anonymous"></script>
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300;400&family=Roboto+Slab:wght@300;400&display=swap" rel="stylesheet">
	</head>
	<body>
	<script type="text/javascript">
		const dtocApp = new dtoc();
	</script>
	</body>
</html>
```

## Loading Documents

There are two approaches for loading documents into DToC.

### Constructor With Input Configuration

Provide a configuration object with an `input` entry to the constructor.
```js
new dtoc({
	"dtocRootUrl": "https://calincs.gitlab.io/cwrc/dtoc/",
	"input": {
		"documents": "//div[@type='chapter']|//div[@type='preface']|//div[@type='afterword']",
		...
		"inputs": [
			"https://calincs.gitlab.io/cwrc/dtoc/dtoc-2.0/data/xml/sample.xml"
		]
	}
});
```

### Calling Methods After Initialization

Call `load` with an input configuration.
```js
const dtocApp = new dtoc();
dtocApp.load({
	"documents": "//div[@type='chapter']|//div[@type='preface']|//div[@type='afterword']",
	...
	"inputs": [
		"https://calincs.gitlab.io/cwrc/dtoc/dtoc-2.0/data/xml/sample.xml"
	]
})
```

Call `showInputWindow` (with an optional input configuration).
```js
const dtocApp = new dtoc();
dtocApp.showInputWindow();
```

## Configuration Object Details

The configuration object contains app configuration as well as the input configuration.

_Italicized properties are optional_

### Configuration Object

| Name | Type | Details |
| ---- | ---- | ------- |
| _dtocRootUrl_ | String | An URL specifying the location of the DToC files root. If not provided then `window.location` will be used. |
| _input_ | Object | The input configuration object (see below for details) |
| _showMenu_ | Boolean | Whether to show the options menu. Default is true. |
| _debug_ | Boolean | Whether to enable debug messages in the console. |


### Input Configuration Object

| Name | Type | Details |
| ---- | ---- | ------- |
| inputs | Array | An array of URLs or Files |
| documents | String | The XPath for locating the document(s) within each XML file |
| _documentContent_ | String | The XPath for locating the actual content within the document |
| _documentTitle_ | String | The XPath for locating the document title |
| _documentAuthor_ | String | The XPath for locating the document author |
| _documentImages_ | String | The XPath for identifying images |
| _documentNotes_ | String | The XPath for identifying notes |
| _documentLinks_ | String | The XPath for identifying links |
| _ignoreNamespace_ | Boolean | Whether to remove the root/default namespace prior to evaluating XPaths. Use to avoid having to use local-name() in XPaths. |
| _editionTitle_ | String | The title of the edition |
| _editionSubtitle_ | String | The subtitle of the edition |
| _indexDocument_ | String | The XPath for locating the index document to use as the corpus index |
| _curation_ | Object | An optional curation object. Can be used to customize the tags panel and the table of contents panel. |
| _curation.markup_ | Array | An array of markup |
| _curation.toc_ | Array | The table of contents |

### Get the Input Configuration Object

You can get the current input configuration by calling the `getInputConfig` method.
```js
const inputConfig = dtocApp.getInputConfig();
console.log(inputConfig.documents);
```